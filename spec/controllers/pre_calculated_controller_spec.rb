require 'rails_helper'

describe PreCalculatedController do

  before :each do
    request.env["HTTP_ACCEPT"] = 'application/json'
  end

  context 'it receives non numeric parameter' do
    it 'should return 400' do
      get :calculate, position: 'abc'
      expect(response.status).to eq(400)
    end

    it 'should return type error message' do
      get :calculate, position: 'abc'
      expect(response.body).to eq("{\"message\":\"Invalid position. It should be a positive integer number.\"}")
    end
  end

  context 'it receives a negative number' do

    it 'should return 400' do
      get :calculate, position: '-7'
      expect(response.status).to eq(400)
    end

    it 'should return negative number error message' do
      get :calculate, position: '-7'
      expect(response.body).to eq("{\"message\":\"Invalid position. It should be a positive integer number.\"}")
    end

  end

  context 'it receives a valid number for position' do

    let!(:pre_calculated) { create :pre_calculated }
    let!(:pre_calculated_zero) { create :pre_calculated, position: 0, values: ["0"] }

    it 'does not receive 0' do
      get :calculate, position: '0'
      expect(response.status).to eq(400)
    end

    it 'receives a positive integer and returns 200' do
      get :calculate, position: '3'
      expect(response.status).to eq(200)
    end

    it 'returns a list of values' do
      get :calculate, position: '3'
      parsed_body = JSON.parse(response.body)
      expect(parsed_body["list"]).to eq(pre_calculated.values)
    end

  end

  context 'it receives an unprocessable position' do

    let!(:pre_calculated) { create :pre_calculated }
    let!(:pre_calculated_zero) { create :pre_calculated, position: 0, values: ["0"] }

    it 'should return 422' do
      get :calculate, position: '50000'
      expect(response.status).to eq(422)
    end

    it 'should return unprocessed number error message' do
      get :calculate, position: '50000'
      expect(response.body).to eq("{\"message\":\"Number not pre processed. Sorry!\"}")
    end

  end
  
end