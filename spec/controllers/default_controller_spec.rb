require 'rails_helper'

describe DefaultController do

  before :each do
    request.env["HTTP_ACCEPT"] = 'application/json'
  end

  context 'it receives non numeric parameter' do
    it 'should return 400' do
      get :calculate, position: 'abc'
      expect(response.status).to eq(400)
    end

    it 'should return type error message' do
      get :calculate, position: 'abc'
      expect(response.body).to eq("{\"message\":\"Invalid position. It should be an integer number.\"}")
    end
  end

  context 'it receives a negative number' do

    it 'should return 400' do
      get :calculate, position: '-7'
      expect(response.status).to eq(400)
    end

    it 'should return negative number error message' do
      get :calculate, position: '-7'
      expect(response.body).to eq("{\"message\":\"Invalid position. It should be a positive number.\"}")
    end

  end

  context 'it receives a valid number for position' do

    it 'does not receive 0' do
      get :calculate, position: '0'
      expect(response.status).to eq(400)
    end

    it 'receives a positive integer and returns 200' do
      get :calculate, position: '3'
      expect(response.status).to eq(200)
    end

    it 'returns a list of values' do
      get :calculate, position: '3'
      expect(response.body).to eq("{\"list\":[0,1,1]}")
    end

  end

  context 'it receives an unprocessable position' do

    it 'should return 422' do
      get :calculate, position: '50000'
      expect(response.status).to eq(422)
    end

    it 'should return unprocessed number error message' do
      get :calculate, position: '50000'
      expect(response.body).to eq("{\"message\":\"Number is too big. Try PreComputed.\"}")
    end

  end
  
end