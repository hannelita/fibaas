require 'rails_helper'
describe FibonacciService do
  
  describe 'initialize' do
    
    context 'position smaller than 3' do
      subject { FibonacciService.new(2) }

      it 'should perform a position offset' do
        expect(subject.instance_variable_get(:@position)).to eq(1)
      end

    end

  end

  describe 'calculate' do

    context 'position eq 1' do

      subject { FibonacciService.new(1).calculate }

      it 'should have an array of the same position size' do
        expect(subject.results.size).to eq(1)
      end

      it 'match the output' do
        expect(subject.results).to match_array [0]
      end

      it 'should have a successful result' do
        expect(subject.successful?).to be_truthy
      end

      it 'should have no errors on result' do
        expect(subject.errors).to be_nil
      end

    end

    context 'position eq 2' do

      subject { FibonacciService.new(2).calculate }

      it 'should have an array of the same position size' do
        expect(subject.results.size).to eq(2)
      end

      it 'match the output' do
        expect(subject.results).to match_array [0, 1]
      end

      it 'should have a successful result' do
        expect(subject.successful?).to be_truthy
      end

      it 'should have no errors on result' do
        expect(subject.errors).to be_nil
      end

    end

    context 'position eq 3' do

      subject { FibonacciService.new(3).calculate }

      it 'should have an array of the same position size' do
        expect(subject.results.size).to eq(3)
      end

      it 'match the output' do
        expect(subject.results).to match_array [0, 1, 1]
      end

      it 'should have a successful result' do
        expect(subject.successful?).to be_truthy
      end

      it 'should have no errors on result' do
        expect(subject.errors).to be_nil
      end

    end

    context 'position greater than 3, smaller than 200, eq 5' do

      subject { FibonacciService.new(5).calculate }

      it 'should have an array of the same position size' do
        expect(subject.results.size).to eq(5)
      end

      it 'match the output' do
        expect(subject.results).to match_array [0, 1, 1, 2, 3]
      end

      it 'should have a successful result' do
        expect(subject.successful?).to be_truthy
      end

      it 'should have no errors on result' do
        expect(subject.errors).to be_nil
      end

    end

    context 'position greater than 3, smaller than 200, eq 6' do

      subject { FibonacciService.new(6).calculate }

      it 'should have an array of the same position size' do
        expect(subject.results.size).to eq(6)
      end

      it 'match the output' do
        expect(subject.results).to match_array [0, 1, 1, 2, 3, 5]
      end

      it 'should have a successful result' do
        expect(subject.successful?).to be_truthy
      end

      it 'should have no errors on result' do
        expect(subject.errors).to be_nil
      end

    end

    context 'position greater than 200' do

      subject { FibonacciService.new(500).calculate }

      it 'should have no results' do
        expect(subject.results).to be_nil
      end

      it 'should have a successful result' do
        expect(subject.successful?).to be_falsey
      end

      it 'should have no errors on result' do
        expect(subject.errors).to eq('Number is too big. Try PreComputed.')
      end

    end

  end

end