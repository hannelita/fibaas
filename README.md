# Fibonacci as a Service

Welcome to Fibonacci as a Service v. 1.0.

The idea of this project is providing a list of values corresponding to a Fibonacci Sequence given a 
position as an input. 

This project is written in Ruby on Rails, and it will provide you JSON files via API calls.

## Start consuming

A simple version of this project is deployed on [Heroku](http://heroku.com/).

Open your browser and access [https://fibonacci-aas.herokuapp.com/](https://fibonacci-aas.herokuapp.com/). 

You will notice a plain Welcome page.

### Default Consumption

You can access an endpoint where you will have calculated in Runtime by a default implementation of a Fibonacci Sequence Algorithm. Open your terminal and type, for example:
```
curl -X GET https://fibonacci-aas.herokuapp.com/default/5.json
```

You will obtain the following JSON as a response:

```
{"list":[0,1,1,2,3]}
```

Try different values.

Attention: Only positive integers numbers are allowed.
Also, to avoid a slow response time (and possibly a timeout from the server), only values up to 200 are accepted.

## I need bigger values! Precalculated Consumption

No worries! Try ```/precalculated```

For example:

```
curl -X GET https://fibonacci-aas.herokuapp.com/precalculated/10.json
```

And you should get the following JSON response:
```
{"list":["0","1","1","2","3","5","8","13","21","34"]}

```

Precalculated values will fetch the results from a local database. For the Heroku App, values up to 3000 are precomputed.
Also, Precomputed list value comes as a list of Strings, whereas ```/default``` delivers you numeric values. 
(It is easier to store strings instead of big floats of huge numbers). 
Just be aware of this type difference.

## Running locally

Clone the project. You need Ruby 2.3.1 installed. You can easily get it with [RVM](https://rvm.io/). Just run ```rvm install 2.3.1```. After installing Ruby, install [Bundler](http://bundler.io/), with ```gem install bundler```. PostgreSQL 9.2 or newer is required as well. You can download it [here](https://www.postgresql.org/download/)

On your terminal, run ```bundle install```. When it is done, start Rails by running ```rails s```.

You can try the ```/default``` endpoint now.

To have the Precalculated values, you need to do the following:

1. Open another tab and run ```rake db:create:all```and then ```rake db:migrate```. This will set up your local database.

2. You will need a running instance of Sidekiq. Open another tab and type ```bundle exec sidekiq```

3. You will also need Redis Server. You can get it [here](http://redis.io/download) or via any package manager. Start it by opening another tab and running ```redis-server```.

4. Start Rails console by running ```rails c```.  Invoke the Sidekiq worker by running, on Rails Console, the following command: ```::CreatePreCalculatedValuesWorker.perform_async 3000```. Wait a couple minutes. This should populate your local database with the first 3000 lists for the first 3000 Fibonacci Sequence positions.

Now you can invoke ```/precalculated``` API.

### A note on status and formats.

Every time you send an invalid value (example: a negative number as a position), you will get a 400 or a 422, within an appropriate error message. 

Every time the API can calculate the value you asked and send you back a result list, it will respond with 200.

At this point, only JSON communication is supported. However, Rails is easily expandable. At the controllers, a multiple responder is already implemented, so if you want to provide HTML or XML responses, just add an extra format line. For example:


```ruby
# /app/controllers/default_controller.rb
def handle_result(result)
    if result.successful?
      respond_to do |format|
        format.json { render json: { list: result.results }, status: 200}
       # format.xml or
       #format.html
      end
    else
      # ...
    end
  end
```

## Running the tests

Open a tab into the project root and run ```rspec spec```.

# TO-DOs (because projects usually can be improved)
- A CSS for the home page on Heroku
- Add HTML support and some pages to provide input from the browser
- Add a CI (it is always good to make sure the tests are ok after a commit to master branch)
- 404 and 50x error pages - because Heroku may go offline or a page may not exist
- Add Devise and Login support. Why? Well, we can trace the user position input sequence and maybe suggest him/her a pattern.
- Persist user data and provide a JSON with suggestions. Example: assume an individual user entered with the following position values: 5-10-15-20. We can suggest him subsequent inputs at once when he/she enters position 25:
```
{"position": 25, "list":[..], "inferred": [{"position": 30, "list":[..]}, {"position": 35, "list":[..]}]}
```

This approach could save user's time and API hits.

Thanks for using Fibonacci as a Service! 
