class CreatePreCalculateds < ActiveRecord::Migration
  def change
    create_table :pre_calculateds do |t|

      t.integer :position, unique: true
      t.text :values, array: true, default: []

      t.timestamps null: false
    end
  end
end
