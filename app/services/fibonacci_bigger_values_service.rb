class FibonacciBiggerValuesService < FibonacciService
  def initialize(position)
    @initial = [0]
    @cache = {}
    @position = offset(position)
  end

  def calculate_bigger_values
    calculate_default
    results
  end
end