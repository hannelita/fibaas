class FibonacciService

  Result = Struct.new(:successful?, :results, :errors)

  def initialize(position)
    @initial = [0]
    @cache = {}
    @position = offset(position)
  end

  def calculate
    if @position < 3
      return_initials(@position)
      Result.new(true, results, nil)
    elsif @position.between?(3,200)
      calculate_default
      Result.new(true, results, nil)
    else
      Result.new(false, nil, "Number is too big. Try PreComputed.")
    end
  end

  def results
    @initial.sort
  end

  protected

  def calculate_default
    add_first_result
    fibonacci(@position)
    concat_cache
  end


  private

  def add_first_result
    @initial.concat [1]
  end

  def return_initials(position)
    case @position
    when 0
      @initial
    when 1
      add_first_result
    when 2
      @initial.concat [1,1]
    end
  end

  def offset(position)
    position - 1
  end

  def fibonacci(n, cache = {})
    if n == 0 || n == 1
      return n
    end
    @cache[n] ||= fibonacci(n-1, @cache) + fibonacci(n-2, @cache)
  end

  def concat_cache
    @cache.each { |k,v| @initial << v }
  end

end