class CreatePreCalculatedValuesWorker
  include Sidekiq::Worker
  def perform(upper_bound=5)
    @upper_bound = upper_bound
    results = FibonacciBiggerValuesService.new(@upper_bound).calculate_bigger_values
    while @upper_bound > 0
      PreCalculated.create(position: @upper_bound, values: results)
      @upper_bound = @upper_bound - 1
      results.pop
    end
  end
end