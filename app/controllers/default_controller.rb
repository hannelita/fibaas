class DefaultController < ApplicationController

  respond_to :json

  def calculate
    position = params[:position]
    check_type(position)
  end

  private
  
  def check_type(position)
    position = number_or_nil(position)
    if position
      check_value(position)
    else
      respond_to do |format|
        message = 'Invalid position. It should be an integer number.'
        format.json { render json: { message: message }, status: 400 }
      end
    end
  end

  def check_value(position)
    if position <= 0 
      respond_to do |format|
        message = 'Invalid position. It should be a positive number.'
        format.json { render json: { message: message }, status: 400}
      end
    else
      result = FibonacciService.new(position).calculate
      handle_result(result)
    end
  end

  def handle_result(result)
    if result.successful?
      respond_to do |format|
        format.json { render json: { list: result.results }, status: 200}
      end
    else
      respond_to do |format|
        format.json { render json: { message: result.errors }, status: 422}
      end
    end
  end

  def number_or_nil(string)
    Integer(string || '')
  rescue ArgumentError
    nil
  end

end
