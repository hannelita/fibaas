class PreCalculatedController < ApplicationController

  respond_to :json

  def calculate
    position = check_type(params[:position])
    unless position
      respond_to do |format|
        message = 'Invalid position. It should be a positive integer number.'
        format.json { render json: { message: message }, status: 400 }
      end
    else
      result = PreCalculated.where(position: position.to_i)
      result = result.first if result
      respond_to do |format|
        if (result.try(:values).try("empty?") || result.nil?) 
          format.json { render json: { message: "Number not pre processed. Sorry!" }, status: 422 }
        else
          format.json { render json: { list: result.values }, status: 200 }
        end
      end
    end
  end

  private

  def check_type(position)
    position = number_or_nil(position)
    if position
      position <= 0 ? nil : position
    end
  end

  def number_or_nil(string)
    Integer(string || '')
  rescue ArgumentError
    nil
  end

end
